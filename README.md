# TWA boilerplate

TWA template code for server & android with instructions to build an unqualified TWA android app with a custom splash screen.

## Dependencies

### Install Nodejs, npm (Skip if already done)

### Make sure that you have owner permissions for this path (Skip if already done)

You may try `sudo chown -R [user]:[user] /usr/local/lib/node_modules` on *nix based systems.

### Install Bubblewrap (without sudo) (Skip if already done)

Bubblewrap version 1.13.2 is used in this project.

`npm install -g @bubblewrap/cli`
 
### Create project (Skip if already exists)

`bubblewrap init --manifest https://<host>/manifest.json`

Note: The manifest.json file referred on above url must have a response status ok. Verify with `curl -I https://<host>/manifest.json`

## Host

Serve files from `web/` in root directory of the host domain which this app will use. 

## Build

### Info

- You can skip manifest.json & robots.txt if there already exists one but make sure they have correct information that we need i.e icon path, robots/crawlers to access `.well-known/`. 
- You need to provide url of icons in `android/twa-manifest.json`; either you can provide url of icons in your domain's `https://<host>/icons/icon-512.png` if you hosted files in `/web` of this repo or provide url path to an icon of your choice.
- Keeping `.well-known/assetlinks.json` is absolutely necessary in hosted domain's root. If you're using the same android keystore for signing then you might not need to make any changes to `assetlinks.json`. If you have more apps that use the same domain, then you can add their details in the array of `assetlinks.json`
### Modify

- Modify changes you require in `twa-manifest.json`.
- Do not forget to replace keystore path and alias name. The one I created has alias name 'brij_qr' and password 'password'.

#### Verify

This should not happen, but as a precaution do the following:

- If you find same old values from twa-manifest in android directory (Could be `example.brij.tech`), then replace them with your new ones (find yourself). 

Note: You can also regenerate everything with bubblewrap instead of modifying values if you know what you're doing.

### Build project

`bubblewrap build` (recommended) 

Note: If `bubblewrap build` fails then try `bubblewrap build --skipPwaValidation`.

## Splashhh

If you want a custom splash then when the `bubblewrap build` prompts for password, do the following:

- Remove splash.png from all drawable folders in android resources.
- Create a drawable folder in resources
- Add your image to that folder with name splash.png or splash.jpg or splash.xml based on it's type (`assets/splash.jpg` is our one).
- Goto LauncherActivity and add this code to that class:

```java
   @NonNull
    protected ImageView.ScaleType getSplashImageScaleType() {
        return ImageView.ScaleType.CENTER_CROP;
    }

```

Also add these imports in the same file:

```java
import android.widget.ImageView;

import androidx.annotation.NonNull;
```

- Now type the password in bubblewrap's prompt and it'll build the APK
